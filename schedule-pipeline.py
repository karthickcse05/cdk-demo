import gitlab
import os

def create_pipeline(gitlab_project,gitlab_branch,description,variable,cron_value):
    schedule_pipeline = gitlab_project.pipelineschedules.create({
        'ref': gitlab_branch,
        'description': description,
        'cron': cron_value
    })
    schedule_pipeline.variables.create({'key': 'TEST', 'value': variable})

def check(list,element):
    for i in list:
         if i.ref==element:
             return True
    return False

def create_scheduled_pipeline(gitlab_project,gitlab_branch):
    # Scheduled Pipeline 
    create_pipeline(gitlab_project, gitlab_branch, 'CDK Demo '+ gitlab_branch + ' branch scheduled pipeline',
                    'demo-value','30 6 * * 1-5')
    
if __name__ == '__main__':
    token = os.getenv("GitLab_Token")
    project_id = os.getenv("Project_ID")
    branch = os.getenv("Branch")
    gl = gitlab.Gitlab(url='https://gitlab.com/', private_token=token)
    project = gl.projects.get(project_id);

    schedules = project.pipelineschedules.list();
    if len(schedules) > 0 :
        if check(schedules,branch):
            print("Scheduled pipelines are already created for the branch " + branch + ". Kindly check in Gitlab.")
        else:
            create_scheduled_pipeline(project, branch)
            print("Scheduled pipeline created successfully for the branch " + branch +".")
    else:
        create_scheduled_pipeline(project,branch)
        print("Scheduled pipeline created successfully for the branch " + branch +".") 
